package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

import static net.sf.jasperreports.engine.JasperExportManager.exportReportToPdfFile;

public class ExporterController {

    public Label pathLabel;
    private File fileToConvert;
    private Stage primaryStage;

    @FXML
    private void choseFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file...");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Jasper report file", "*.jrprint"));

        fileToConvert = fileChooser.showOpenDialog(primaryStage);
        if (fileToConvert != null) {
            pathLabel.setText(fileToConvert.toString());
        }
    }

    @FXML
    private void exportToPdf(ActionEvent event) {
        if (fileToConvert != null) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export to pdf");
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("PDF file", "*.pdf"));

            File outputFile = fileChooser.showSaveDialog(primaryStage);
            if (outputFile != null) {
                try {
                    exportReportToPdfFile(fileToConvert.getAbsolutePath(), outputFile.getAbsolutePath());
                } catch (Exception e) {
                    Alert convertError = new Alert(Alert.AlertType.ERROR);
                    convertError.setHeaderText("Something went wrong:)");
                    convertError.setContentText(e.getMessage());
                    convertError.showAndWait();
                }
            }
        }
    }

    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void showStage() {
        primaryStage.show();
    }
}